# Ear Disease Diagnosis Server Application
### Running

Dillinger requires [Java](https://www.java.com/en/) v1.1+ to run.

```sh
$ java -jar diagnosis-server.jar
```