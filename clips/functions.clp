;; Shared functions

(deffunction diagnose-infections (?infection)
    (call ?*i-diag-di* addInfection ?infection))

(deffunction clear-diagnosed-infections ()
    (call ?*i-diag-di* clear))