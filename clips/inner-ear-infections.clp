;; Rules for inner ear infections

;; Labyrinthitis
(defrule labyrinthitis
    (patient {has_a_sudden_onset_of_dizziness == "yes"})
    (or
        (patient {ear_infections_had_in_the_past_year == "1_to_3"})
        (patient {ear_infections_had_in_the_past_year == "4_or_more"})
    )
    (or
        (patient {conditions_diagnosed_with == "meningitis"})
        (patient {conditions_diagnosed_with == "tumours"})
    )
    (patient {has_felt_hot_or_feverish == "yes"})
    (patient {feels_pressure_in_ears == "yes"})
    (patient {has_had_any_cold_or_allergy_symptoms == "yes"})
    =>
    (bind ?res (run-query find-infection-by-code "LTS"))
    (while (?res hasNext)
        (diagnose-infections (call ?res next))
        )
    )
