;; Rules for middle ear infections

;; Otitis Media
(defrule otitis_media
    (patient {appetite == "lack_of_appetite"})
    (or
        (patient {smokes_or_has_ever_smoked_tobacco == "formerly"})
        (patient {smokes_or_has_ever_smoked_tobacco == "currently"})
    )
    (or
        (patient {conditions_diagnosed_with == "cleft_palate"})
        (patient {conditions_diagnosed_with == "downs_syndrome"})
    )
    (or
        (patient {ear_infections_had_in_the_past_year == "1_to_3"})
        (patient {ear_infections_had_in_the_past_year == "4_or_more"})
    )
    (patient {has_a_sudden_onset_of_dizziness == "yes"})
    (patient {has_felt_hot_or_feverish == "yes"})
    (patient {has_discomfort_when_lying_down == "yes"})
    (patient {has_running_nose == "yes"})
    (patient {has_diarrhoea == "yes"})
    (patient {feels_itchy_in_ears == "yes"})
    =>
    (bind ?res (run-query find-infection-by-code "OTM"))
    (while (?res hasNext)
        (diagnose-infections (call ?res next))
        )
    )

;; Tinnitus
(defrule tinnitus
    (or
        (patient {age == "more_than_65"})
        (patient {age == "56_to_65"})
    )
    (or
        (patient {conditions_diagnosed_with == "epilepsy"})
        (patient {conditions_diagnosed_with == "high_blood_pressure"})
    )
    (or
        (patient {ear_infections_had_in_the_past_year == "1_to_3"})
        (patient {ear_infections_had_in_the_past_year == "4_or_more"})
    )
    (patient {is_exposed_to_loud_sounds == "yes"})
    (patient {has_had_head_injury == "yes"})
    =>
    (bind ?res (run-query find-infection-by-code "TNT"))
    (while (?res hasNext)
        (diagnose-infections (call ?res next))
        )
    )

;; Hyperacusis
(defrule hyperacusis
    (or
        (patient {conditions_diagnosed_with == "epilepsy"})
        (patient {conditions_diagnosed_with == "high_blood_pressure"})
    )
    (or
        (patient {ear_infections_had_in_the_past_year == "1_to_3"})
        (patient {ear_infections_had_in_the_past_year == "4_or_more"})
    )
    (patient {is_exposed_to_loud_sounds == "yes"})
    =>
    (bind ?res (run-query find-infection-by-code "HYP"))
    (while (?res hasNext)
        (diagnose-infections (call ?res next))
        )
    )