;; Rules for outer ear infections

;; Mastoiditis
(defrule mastoiditis
    (or
        (patient {ear_infections_had_in_the_past_year == "1_to_3"})
        (patient {ear_infections_had_in_the_past_year == "4_or_more"})
    )
    (patient {discharge_from_ear == "yes"})
    (patient {has_felt_hot_or_feverish == "yes"})
    (patient {has_swelling_behind_the_ear == "yes"})
    =>
    (bind ?res (run-query find-infection-by-code "MST"))
    (while (?res hasNext)
        (diagnose-infections (call ?res next))
        )
    )

;; Otitis Externa
(defrule otitis_externa
    (or
        (patient {ear_infections_had_in_the_past_year == "1_to_3"})
        (patient {ear_infections_had_in_the_past_year == "4_or_more"})
    )
    (patient {discharge_from_ear == "yes"})
    (patient {has_gone_swimming == "yes"})
    (patient {has_swelling_behind_the_ear == "yes"})
    (patient {feels_itchy_in_ears == "yes"})
    (patient {has_sore_throat == "yes"})

    =>
    (bind ?res (run-query find-infection-by-code "OTE"))
    (while (?res hasNext)
        (diagnose-infections (call ?res next))
        )
    )