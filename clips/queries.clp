(defquery find-infection-by-code
    (declare (variables ?code))
    (Infection (code ?code))
    )