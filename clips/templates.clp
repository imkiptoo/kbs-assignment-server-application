;; Use Java API to create templates

;; Import the beans
(import assignment.groupten.memory.*)

;; Assign template names and declarations
(deftemplate infection (declare (from-class Infection)))

;; Define class then template
(defclass patient Patient)
(defclass diagnosed-infections DiagnosedInfections)

;; Create new instance and add it to working memory
;; Make new instance global
(defglobal ?*i-diag-patient* = (new Patient "patient" "i-diag-patient-USR1"))
(definstance patient ?*i-diag-patient* dynamic)
(defglobal ?*i-diag-di* = (new DiagnosedInfections))
(definstance diagnosed-infections ?*i-diag-di* dynamic)