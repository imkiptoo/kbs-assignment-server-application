package assignment.groupten;

import assignment.groupten.memory.Infection;
import assignment.groupten.memory.Patient;

import java.util.ArrayList;
import java.util.Collection;

public class DataLoader {

    public static ArrayList<Infection> infections;

    public static Patient loadPatient(){
        return new Patient("patient", "i-diag-patient-USR1");
    }

    public static Collection loadInfections(){
        infections = new ArrayList<>();

        infections.add(new Infection(
                "Tinnitus",
                "TNT",
                "Tinnitus",
                "Tinnitus is the perception of sound in the ears or head not caused by an external sound source. Ringing and buzzing sounds may be heard in one or both ears or appear to be generally in the head region but can be variable and difficult to decide exactly where it seems to be.",
                "Ringing in the ears\n" +
                        "A high-pitched whistling, buzzing, ringing, humming or 'roaring ocean' sound in one or both ears\n" +
                        "Often due to the pressure changes associated with moving your head or lying down\n" +
                        "When you're feeling tired stressed or are in noticeably  quiet surroundings\n" +
                        "The noise coming from a television or radio to be painfully loud, despite it being set at a 'normal' volume.\n" +
                        "Have you been running an air conditioner permanently in the office?",
                "Exposure to loud noises\n" +
                        "Ear infections\n" +
                        "Head injury\n" +
                        "Waxy build-up in the ear\n" +
                        "Side-effect of medication\n" +
                        "High blood pressure\n" +
                        "Age-related hearing loss\n" +
                        "Epilepsy",
                "Medications\n" +
                        "Drugs can't cure tinnitus, but in some cases they may help reduce the severity of symptoms or complications. Possible medications include the following:\n" +
                        "Tricyclic antidepressants, such as amitriptyline and nortriptyline, have been used with some success. However, these medications are generally used for only severe tinnitus, as they can cause troublesome side effects, including dry mouth, blurred vision, constipation and heart problems.\n" +
                        "Alprazolam (Xanax) may help reduce tinnitus symptoms, but side effects can include drowsiness and nausea. It can also become habit-forming.\n" +
                        "\n" +
                        "Lifestyle and home remedies\n" +
                        "Often, tinnitus can't be treated. Some people, however, get used to it and notice it less than they did at first. For many people, certain adjustments make the symptoms less bothersome. These tips may help:\n" +
                        "Avoid possible irritants. Reduce your exposure to things that may make your tinnitus worse. Common examples include loud noises, caffeine and nicotine.\n" +
                        "Cover up the noise. In a quiet setting, a fan, soft music or low-volume radio static may help mask the noise from tinnitus.\n" +
                        "Manage stress. Stress can make tinnitus worse. Stress management, whether through relaxation therapy, biofeedback or exercise, may provide some relief.\n" +
                        "Reduce your alcohol consumption. Alcohol increases the force of your blood by dilating your blood vessels, causing greater blood flow, especially in the inner ear area.\n" +
                        "\n" +
                        "Coping and support\n" +
                        "Tinnitus doesn't always improve or completely go away with treatment. Here are some suggestions to help you cope:\n" +
                        "Counseling. A licensed therapist or psychologist can help you learn coping techniques to make tinnitus symptoms less bothersome. Counseling can also help with other problems often linked to tinnitus, including anxiety and depression.\n" +
                        "Support groups. Sharing your experience with others who have tinnitus may be helpful. There are tinnitus groups that meet in person, as well as internet forums. To ensure that the information you get in the group is accurate, it's best to choose a group facilitated by a physician, audiologist or other qualified health professional.\n" +
                        "Education. Learning as much as you can about tinnitus and ways to alleviate symptoms can help. And just understanding tinnitus better makes it less bothersome for some people.\n",
                "Does tinnitus cause hearing loss?\n" +
                        "Tinnitus is not a disease itself or a cause of hearing loss. It is a symptom that something is wrong somewhere in the auditory system, which can include the cochlea of the inner ear, the auditory nerve and the areas of the brain that process sound. In about 90% of cases, it accompanies hearing loss and an individual can have both hearing loss and tinnitus from noise damage. However the two do not always occur together. It is possible to have no measurable hearing loss but suffer from the condition.\uFEFF\uFEFF"
        ));

        infections.add(new Infection(
                "Otitis Media",
                "OTM",
                "Otitis Media",
                "Otitis Media is an inflammation or infection of the middle ear estimated to affect at least one in four young people before they reach their teenage years. Approximately 75% of cases occur in children under the age of ten. Infants between six and 15 months old are the most commonly affected, but as an adult you can also suffer from Otitis Media. Most cases pass within a few days without any need to consult your local GP, but if you or your child suffer from any other underlying health conditions or see no signs of improvement within a week, we highly recommend you contact your local GP for further medical advice.",
                "Pulling, tugging or rubbing of the ear\n" +
                        "Feverish temperature\n" +
                        "Lack of appetite\n" +
                        "Restlessness when trying to sleep\n" +
                        "Irritability\n" +
                        "A lack of response to quiet sounds\n" +
                        "Vomiting and/or diarrhoea\n" +
                        "Earache\n" +
                        "Runny nose\n" +
                        "Loss of balance",
                "Frequent nose or throat infections\n" +
                        "Attending a nursery where infection exposure is likely to be higher\n" +
                        "Exposure to tobacco smoke (passive smoking)\n" +
                        "Feeding your children when they're flat on their back\n" +
                        "Having a cleft palate\n" +
                        "Having Down's syndrome",
                "If you suspect that you or your child has Otitis Media, it can normally be identified using an otoscope, a medical device that has a magnifying glass attached to one end so your GP can look inside your ear. Your local GP will also be able to look for a bulging ear drum, an unusual colour or any perforations in the eardrum.\n" +
                        "When the infection has been diagnosed, you may find that you are simply asked to take antibiotics and painkillers. Antibiotics will normally only be considered if you or your child has more severe symptoms such as earache and/or ear discharge.",
                "Middle ear infections are usually a result of a malfunction of the eustachian tube, a canal that links the middle ear with the throat area. The eustachian tube helps to equalize the pressure between the outer ear and the middle ear. When this tube is not working properly, it prevents normal drainage of fluid from the middle ear, causing a build up of fluid behind the eardrum. When this fluid cannot drain, it allows for the growth of bacteria and viruses in the ear that can lead to acute otitis media."
        ));

        infections.add(new Infection(
                "Otitis Externa",
                "OTE",
                "Otitis Externa",
                "Also called swimmer's ear\n" +
                        "Otitis Externa is a condition that causes inflammation of the external ear canal, the tube between your outer ear and your eardrum. It is often referred to as 'swimmers ear', because a common cause is water remaining in the ear canal after swimming. With treatment, any symptoms should clear up within a few days, but some severe cases it may persist for several months or longer, despite normally only affecting one ear at any given time.",
                "Ear pain, varying in severity according to the intensity of your infection\n" +
                        "An itchy feeling in your ear canal\n" +
                        "Temporary hearing loss, or difficulty understanding quiet sounds\n" +
                        "Experience some discharge from your ear, normally a clear, white or yellowy in colour\n" +
                        "Redness and swelling of your outer ear and ear canal\n" +
                        "Tenderness when moving your ear or jaw\n" +
                        "Swollen and/or sore throat glands",
                "A bacterial infection, usually by strains of pseudomonas aeruginosa or staphylococcus aureus.\n" +
                        "Allergic reaction to ear medication, ear plugs, certain shampoos or cosmetics.\n" +
                        "A fungal infection, is more likely if you use antibacterial or steroid ear drops for an extended period of time\n" +
                        "Discharge from a middle ear infection (otitis media).\n" +
                        "Seborrhoeic dermatitis - a common skin condition where naturally greasy areas of the skin become inflamed.\n" +
                        "Overexposure to moisture - swimming (especially in dirty water), sweating or humid environments can all introduce bacteria-laden liquid to the delicate ear canal, or wash away protective layers of earwax.\n" +
                        "Ear damage - most often caused by the insertion of cotton buds, or incorrect insertion of ear plugs or earphones.\n" +
                        "Chemicals - such as hair spray, hair dye or some earwax softeners.\n" +
                        "Underlying skin conditions - such as psoriasis, eczema and acne.\n" +
                        "Weak immune system or allergic conditions - such as asthma, diabetes or as a result of using certain cancer treatments such as chemotherapy.",
                "Otitis Externa can usually be remedied with a simple course of eardrops, as prescribed by your local GP. If your symptoms linger or your case has been particularly severe, you may be referred to a specialist who may undertake, micro-suction or dry swabbing to remove earwax and other debris to make your drops more effective. Severe cases may require an earwick, a plug made from soft cotton gauze that helps insert medication into your ear.\n" +
                        "While you take your medication, it is important to take certain steps at home to help aid your recovery. Avoid getting your ear wet by wearing a shower cap when you bathe, and gently remove any discharge by gently swabbing around your ear rather than in it. Remove any hearing aids, ear plugs and earrings will also help prevent the spread of bacteria.\n" +
                        "If you are experiencing symptoms of Otitis Externa, it is important to make an appointment with your local doctor. To learn more about other ear infections, visit our Otitis Media, Labyrinthitis or ear infection pages.\uFEFF",
                "A number of preventive measures have been recommended, including use of earplugs while swimming, use of hair dryers on the lowest settings and head tilting to remove water from the ear canal, and avoidance of self-cleaning or scratching the ear canal. Acetic acid 2% (Vosol) otic solutions are also used, either two drops twice daily or two to five drops after water exposure. However, no randomized trials have examined the effectiveness of any of these measures."
        ));

        infections.add(new Infection(
                "Mastoiditis",
                "MST",
                "Mastoiditis",
                "Mastoiditis is a bacterial infection that affects the mastoid, a delicate bone behind the ear and is the rarest of all ear infections. Its structure is similar to that of a honeycomb, helping to maintain air space in the middle ear. When it becomes infected or inflamed, the porous bone begins to break down, causing visible swelling.\n" +
                        "The infection normally occurs as a result of a persistent middle ear infection, and can spread outside of the mastoid bone, causing multiple complications if you do not deal with it quickly. The infection is most common in children, although as an adult, you can be affected too. If you or your child has recently suffered from a middle ear infection, take note of your doctor's advice and recovery recommendations to minimise the risks of developing mastoiditis.",
                "Fever\n" +
                        "Irritability\n" +
                        "Lethargy\n" +
                        "Swelling behind your ear\n" +
                        "Redness and tenderness of your ear\n" +
                        "Ear discharge\n" +
                        "A middle ear infection that seemingly hasn't gone away",
                "Bacteria from the middle ear can travel into the air cells of the mastoid bone.\n" +
                        "Less commonly, a growing collection of skin cells called a cholesteatoma, may block drainage of the ear, leading to mastoiditis.",
                "If you suspect that you might have developed mastoiditis, we highly recommend that you seek the advice of a doctor as soon as possible. They will invite you for an initial ear examination, where they will look inside your ear to evaluate your ear's function and check for any inflammation. If they suspect you have an infection, they may recommend further tests to confirm the diagnosis, which may include x-rays, blood tests and swabbed ear-fluid cultures. If your infection is thought to be severe, you may also be sent for a CT or MRI scan.\n" +
                        "In rare cases, it can develop into meningitis when the lining of your brain becomes inflamed.\n" +
                        "Once your local GP has diagnosed the condition you are likely to be prescribed oral antibiotics, eardrops or regular cleaning of the ear. For cases of acute mastoiditis, treatment may take place in your local hospital, where antibiotics will be administered by an IV drip.\n" +
                        "Surgery is also an option, removing the mastoid bone completely or draining your ear. Ear draining is known as a myringotomy, and works by making a small hole in your eardrum to release pressure and allow fluid to escape. An operation to remove your mastoid bone is known as a cortical mastoidectomy, and will only take place if your infection is severe. If you have a severely infected bone and it’s not removed, there is a risk of developing blood clots or brain abscesses, which can be life-threatening.\n" +
                        "After surgery, you will be expected to stay in hospital for a few days, and will not be allowed to get your operated ear wet for at least a week after hospital release.",
                "Swimming is allowed after four to six weeks, depending on how well your surgery has healed. Your local GP will be able to advise on recovery milestones at your check-up appointments."
        ));

        infections.add(new Infection(
                "Labyrinthitis",
                "LTS",
                "Labyrinthitis",
                "Labyrinthitis is an inner ear viral infection, where you can typically experience feelings of vertigo and dizziness. It occurs when the labyrinth - a delicate structure deep within the ear - becomes inflamed, often as a result of exposure to another more common viral illness such as a cold or flu.",
                "Dizziness\n" +
                        "Vertigo\n" +
                        "Hearing loss, ranging from mild to total hearing loss\n" +
                        "Pressure in the affected ear(s)\n" +
                        "Ear pain\n" +
                        "Vomiting and/or nausea\n" +
                        "Blurred or double vision\n" +
                        "Feverish temperatures of 38°C or above",
                "Bacterial infection in the middle ear\n" +
                        "Meningitis\n" +
                        "Ear injury\n" +
                        "Allergies\n" +
                        "Tumours\n" +
                        "Blood circulation blockage to part of the brain\n" +
                        "A side effect of medication (very rare)",
                "For sudden attacks of vertigo that are accompanied by deafness in one ear, it is advised that you seek urgent medical attention, as it could be a sign of blocked blood vessels to the brain or there may be additional problems which may need hearing aid support. For less intense but more persistent cases, your local doctor may prescribe anti-sickness medication to help with the vertigo, or a short course of steroid tablets that will encourage the inflammation to reduce quicker. If symptoms do not clear up within a few weeks, you may be referred for vestibular rehabilitation therapy.\n" +
                        "For incidences of labyrinthitis that have a less common cause, specific treatment will be prescribed. For instance, a bacterial infection in the middle ear might require antibiotics that would be ineffectual for a viral infection.\n" +
                        "If you’re suffering with labyrinthitis, there are a variety of self-help options that might alleviate some of the symptoms. Drinking plenty of water, bed rest and minimising exposure to bright lights, caffeine and loud noises should help ease the dizziness. It is also advised to avoid stressful situations.",
                "In most cases, symptoms will resolve within one to three weeks, and you’ll experience a full recovery in a few months. In the meantime, symptoms such as vertigo and vomiting may interfere with your ability to work, drive, or participate fully in sports. Try to ease back into these activities slowly as you recover.\n" +
                        "If your symptoms haven’t improved after several months, your doctor may want to order additional tests to rule out other conditions if they haven’t already done so.\n" +
                        "Most people only have a single episode of labyrinthitis. It rarely becomes a chronic condition."
        ));

        infections.add(new Infection(
                "Hyperacusis",
                "HYP",
                "Hyperacusis",
                "Hyperacusis is a condition that affects how you perceive sounds. You can experience a heightened sensitivity to particular sounds that are not usually a problem for others. This means loud noises, such as fireworks, and everyday sounds like telephones can feel uncomfortable and sometimes painful. It can vary in its severity, from being a mild inconvenience to a life-changing condition.",
                "A sudden discomfort when hearing particular sounds\n" +
                        "Phonophobia, a fear of noise\n" +
                        "Anxiety\n" +
                        "Depression",
                "Long term noise exposure\n" +
                        "Post-traumatic stress disorder\n" +
                        "A result of an existing medical condition",
                "As it quite often appears as a result of another medical condition, investigating this may be the first step in treatment and is something your GP can start. Once this has been ruled out, you will often undergo sound therapy to approach the problem in a similar way to how tinnitus can be treated. Sound therapy is used to help you become less and less affected by the noises you are sensitive to.\n" +
                        "You may find cognitive behavioural therapy (CBT) can help, particularly if you may suffer from anxiety or depression. Hyperacusis can make these problems worse, or even cause them. CBT helps to address the emotions that come with it, and change them in order to reduce feelings of anxiety.\n" +
                        "People can have hearing loss and sound sensitivity, and hearing aids can be tweaked to allow for amplification without excessive amplification.",
                "Prevention Measures\n" +
                        "Whilst the exact cause of hyperacusis is unknown, you may experience this condition due to damage to your hearing from excessive noise exposure. To prevent this, and other hearing concerns such as hearing loss and tinnitus, there are several steps you can take to ensure you protect your hearing. These include:\n" +
                        "Try listening to music at a reduced volume for shorter periods of time\n" +
                        "Wearing ear protection - for example at concerts, or at work if necessary\n" +
                        "Being aware that extended exposure to sounds above 85 decibels can be damage your hearing."
        ));

        return infections;
    }

    public static Infection findInfectionByCode(String code){
        for (Infection infection : infections){
            if(infection.getCode().equals(code.replaceAll("\"","")))
                return infection;
        }
        return null;
    }

}
