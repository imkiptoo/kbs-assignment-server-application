package assignment.groupten;

import assignment.groupten.memory.Patient;
import jess.*;

import java.util.Iterator;

public class Engine {
    private Rete engine;
    private WorkingMemoryMarker factsLoadedMarker;

    public Engine(){
        try {
            // Create a Jess rule engine
            engine = new Rete();
            engine.reset();

            //Load Infections into working memory
            engine.addAll(DataLoader.loadInfections());

            engine.batch("templates.clp"); // Load templates
            engine.batch("functions.clp"); // Load functions
            engine.batch("queries.clp"); // Load queries
            engine.batch("inner-ear-infections.clp"); // Load inner-ear-infections
            engine.batch("middle-ear-infections.clp"); // Load middle-ear-infections
            engine.batch("outer-ear-infections.clp"); // Load outer-ear-infections

            // Mark end of facts loading for later
            factsLoadedMarker = engine.mark();
            engine.run();

        } catch (JessException e) {
            e.printStackTrace();
        }
    }

    public void run(){
        try {
            engine.run();
        } catch (JessException e) {
            e.printStackTrace();
        }
    }

    public void reset(){
        try {
            engine.reset();
        } catch (JessException e) {
            e.printStackTrace();
        }
    }

    public QueryResult runQuery(String name, Object... values){

        ValueVector vector = new ValueVector();
        for (Object o : values){
            vector.add(o);
        }

        try {
            return engine.runQueryStar(name, vector);
        } catch (JessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void eval(String jessCode){
        try {
            engine.eval(jessCode);
        } catch (JessException e) {
            e.printStackTrace();
        }
    }

    public Iterator getObjects(Class clazz){
        // Return the list of <clazz> created by the rules
        return engine.getObjects(new Filter.ByClass(clazz));
    }

    public Patient getPatientBean(){
        Iterator iterator = getObjects(Patient.class);
        Object o = null;
        for (; iterator.hasNext(); ) {
            o = iterator.next();
        }
        return (Patient)o;
    }

}
