package assignment.groupten;

import assignment.groupten.handlers.CorsHandler;
import assignment.groupten.handlers.GetDiagnosedInfections;
import assignment.groupten.handlers.Reset;
import assignment.groupten.memory.Patient;
import assignment.groupten.utils.FallBack;
import assignment.groupten.utils.InvalidMethod;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.RoutingHandler;
import io.undertow.server.handlers.BlockingHandler;
import io.undertow.util.Methods;

public class Main {
    public static Engine engine;
    public static Patient activePatient;

    public static void main(String[] args) {
        try {
            engine = new Engine();
            activePatient = engine.getPatientBean();

            Undertow server = Undertow.builder()
                    .addHttpListener(3001, "0.0.0.0")
                    .setHandler(Handlers.path()
                            .addPrefixPath("/diagnose-infections", router("di"))
                            .addPrefixPath("/reset", router("reset"))
                            .addPrefixPath("/*", Handlers.routing().add(Methods.OPTIONS, "/",  new CorsHandler()))
                    ).build();
            server.start();

            System.out.println("\nServer started at: http://0.0.0.0:3001\n");
        } catch (Exception e){
            e.printStackTrace();
            System.exit(0);
        }
    }

    private static RoutingHandler router(String basePath){
        switch (basePath){
            case "di":
                return Handlers.routing()
                        .post("/", new BlockingHandler(new GetDiagnosedInfections()))
                        .add(Methods.OPTIONS, "/",  new CorsHandler())
                        .setInvalidMethodHandler(new InvalidMethod())
                        .setFallbackHandler(new FallBack());
            case "reset":
                return Handlers.routing()
                        .get("/", new Reset())
                        .setInvalidMethodHandler(new InvalidMethod())
                        .setFallbackHandler(new FallBack());
            default:
                return Handlers.routing();
        }
    }
}
