package assignment.groupten.handlers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.HttpString;
import io.undertow.util.PathTemplateMatch;
import assignment.groupten.Main;
import assignment.groupten.memory.Patient;
import assignment.groupten.utils.DateTime;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DefaultHandler implements HttpHandler {

    private void handleCors(HttpServerExchange exchange){
        exchange.getResponseHeaders().put(
                new HttpString("Access-Control-Allow-Origin"),
                "*");
        exchange.getResponseHeaders().put(
                new HttpString("Access-Control-Allow-Methods"),
                "POST, GET, OPTIONS, PUT, PATCH, DELETE");
        exchange.getResponseHeaders().put(
                new HttpString("Access-Control-Allow-Headers"),
                "Content-Type, Accept, Authorization");
    }

    protected void send(HttpServerExchange exchange, Object data){
        send(exchange, data, 200);
    }
    protected void send(HttpServerExchange exchange, Object data, Integer status){
        exchange.setStatusCode(status);
        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");

        try {
            exchange.getResponseSender().send(new ObjectMapper().writeValueAsString(data));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    protected String getPathVar(HttpServerExchange exchange, String pathVarId){
        PathTemplateMatch pathMatch =
                exchange.getAttachment(PathTemplateMatch.ATTACHMENT_KEY);
        return pathMatch.getParameters().get(pathVarId);
    }

    @SuppressWarnings("unchecked")
    protected Object getBodyObject(HttpServerExchange exchange, Class clazz){
        String body = getBody(exchange);

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            return objectMapper.readValue(body, clazz);
        } catch (Exception e) {
            e.printStackTrace();
            exchange.addQueryParam("MARSHALL_ERROR",e.getMessage());
            return null;
        }
    }

    public void copyPatientProperties(Patient patient){
        Main.activePatient.setAge(patient.getAge());
        Main.activePatient.setGender(patient.getGender());
        Main.activePatient.setHas_a_sudden_onset_of_dizziness(patient.getHas_a_sudden_onset_of_dizziness());
        Main.activePatient.setHas_felt_hot_or_feverish(patient.getHas_felt_hot_or_feverish());
        Main.activePatient.setHas_discomfort_when_lying_down(patient.getHas_discomfort_when_lying_down());
        Main.activePatient.setAppetite(patient.getAppetite());
        Main.activePatient.setEar_infections_had_in_the_past_year(patient.getEar_infections_had_in_the_past_year());
        Main.activePatient.setDischarge_from_ear(patient.getDischarge_from_ear());
        Main.activePatient.setHas_sore_throat(patient.getHas_sore_throat());
        Main.activePatient.setFeels_itchy_in_ears(patient.getFeels_itchy_in_ears());
        Main.activePatient.setHas_running_nose(patient.getHas_running_nose());
        Main.activePatient.setHas_swelling_behind_the_ear(patient.getHas_swelling_behind_the_ear());
        Main.activePatient.setFeels_pressure_in_ears(patient.getFeels_pressure_in_ears());
        Main.activePatient.setHas_diarrhoea(patient.getHas_diarrhoea());
        Main.activePatient.setIs_exposed_to_loud_sounds(patient.getIs_exposed_to_loud_sounds());
        Main.activePatient.setHas_had_head_injury(patient.getHas_had_head_injury());
        Main.activePatient.setHas_had_any_cold_or_allergy_symptoms(patient.getHas_had_any_cold_or_allergy_symptoms());
        Main.activePatient.setSmokes_or_has_ever_smoked_tobacco(patient.getSmokes_or_has_ever_smoked_tobacco());
        Main.activePatient.setHas_gone_swimming(patient.getHas_gone_swimming());
        Main.activePatient.setConditions_diagnosed_with(patient.getConditions_diagnosed_with());
    }

    private String getBody(HttpServerExchange exchange){
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();

        try {
            exchange.startBlocking();
            reader = new BufferedReader(new InputStreamReader(exchange.getInputStream()));

            String line;
            while((line = reader.readLine()) != null ) {
                builder.append( line );
            }
        } catch(IOException e) {
            e.printStackTrace( );
        } finally {
            if(reader != null) {
                try {
                    reader.close();
                } catch( IOException e ) {
                    e.printStackTrace();
                }
            }
        }

        String body = builder.toString();

        return body;
    }

    private void printRequestInfo(HttpServerExchange exchange){
        String patientAgentHeader = "UNKNOWN";
        try{
            patientAgentHeader = exchange.getRequestHeaders().get("Patient-Agent").getFirst();
        }catch (NullPointerException ignore){}
        String patient = patientAgentHeader.toLowerCase();

        String os = "UNKNOWN";
        String browser = "UNKNOWN";

        // Determine OS
        if (patient.contains("windows")) os = "Windows";
        else if (patient.contains("linux")) os = "Linux";
        else if(patient.contains("mac")) os = "Mac";
        else if(patient.contains("x11")) os = "Unix";
        else if(patient.contains("android")) os = "Android";
        else if(patient.contains("iphone")) os = "IPhone";

        // Determine Browser
        if (patient.contains("msie"))
        {
            String substring= patientAgentHeader.substring(patientAgentHeader.indexOf("MSIE")).split(";")[0];
            browser=substring.split(" ")[0].replace("MSIE", "IE")+"-"+substring.split(" ")[1];
        } else if (patient.contains("safari") && patient.contains("version"))
        {
            browser=(patientAgentHeader.substring(patientAgentHeader.indexOf("Safari")).split(" ")[0]).split("/")[0]+"-"+(patientAgentHeader.substring(patientAgentHeader.indexOf("Version")).split(" ")[0]).split("/")[1];
        } else if ( patient.contains("opr") || patient.contains("opera"))
        {
            if(patient.contains("opera"))
                browser=(patientAgentHeader.substring(patientAgentHeader.indexOf("Opera")).split(" ")[0]).split("/")[0]+"-"+(patientAgentHeader.substring(patientAgentHeader.indexOf("Version")).split(" ")[0]).split("/")[1];
            else if(patient.contains("opr"))
                browser=((patientAgentHeader.substring(patientAgentHeader.indexOf("OPR")).split(" ")[0]).replace("/", "-")).replace("OPR", "Opera");
        } else if (patient.contains("chrome"))
        {
            browser=(patientAgentHeader.substring(patientAgentHeader.indexOf("Chrome")).split(" ")[0]).replace("/", "-");
        } else if ((patient.contains("mozilla/7.0")) || (patient.contains("netscape6"))  || (patient.contains("mozilla/4.7")) || (patient.contains("mozilla/4.78")) || (patient.contains("mozilla/4.08")) || (patient.contains("mozilla/3")) )
        {
            browser = "Netscape-?";

        } else if (patient.contains("firefox"))
        {
            browser=(patientAgentHeader.substring(patientAgentHeader.indexOf("Firefox")).split(" ")[0]).replace("/", "-");
        } else if(patient.contains("rv"))
        {
            browser="IE-" + patient.substring(patient.indexOf("rv") + 3, patient.indexOf(")"));
        }

        System.out.println("\n*********** REQUEST INFO ***********");
        System.out.println("Request URI: " + exchange.getRequestURI());
        System.out.println("Protocol: " + exchange.getProtocol());
        System.out.println("Request Method: " + exchange.getRequestMethod());
        try{
            System.out.println("Remote Address: " + exchange.getRequestHeaders().get("X-Real-IP").getFirst());
        }catch (NullPointerException e){
            System.out.println("Remote Address: "+exchange.getSourceAddress().getAddress().toString());
        }
        System.out.println("Patient-Agent: " + patientAgentHeader);
        System.out.println("Remote OS: " + os);
        System.out.println("Remote Browser: " + browser);
        System.out.println("Timestamp: " + DateTime.getCurrentDateTime());
        System.out.println("**************************************\n");
        handleCors(exchange);
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
//        handleCors(exchange);
        printRequestInfo(exchange);
    }
}
