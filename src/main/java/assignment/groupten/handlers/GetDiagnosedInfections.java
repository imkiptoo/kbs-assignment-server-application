package assignment.groupten.handlers;

import assignment.groupten.Engine;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import io.undertow.util.StatusCodes;
import assignment.groupten.Main;
import assignment.groupten.memory.DiagnosedInfections;
import assignment.groupten.memory.Patient;
import assignment.groupten.utils.ExceptionRepresentation;

import java.util.Iterator;

public class GetDiagnosedInfections extends DefaultHandler {

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        super.handleRequest(exchange);

        Main.engine = new Engine();
        Main.activePatient = Main.engine.getPatientBean();
        Main.engine.eval("(clear-diagnosed-infections)");

        Patient patient = (Patient) getBodyObject(exchange, Patient.class);
        if(patient == null){

            send(exchange, new ExceptionRepresentation(
                    exchange.getQueryParameters().get("MARSHALL_ERROR").getFirst(),
                    exchange.getRequestURI(),
                    "Error: Unable to understand payload.",
                    StatusCodes.INTERNAL_SERVER_ERROR,
                    exchange.getRequestMethod()
            ), StatusCodes.INTERNAL_SERVER_ERROR);
            return;
        }

        copyPatientProperties(patient);
        Main.engine.run();

        Iterator iterator = Main.engine.getObjects(DiagnosedInfections.class);
        DiagnosedInfections diagnosedInfections = (DiagnosedInfections) iterator.next();


        send(exchange, diagnosedInfections.getAll());
        //Main.engine.eval("(clear-diagnosed-infections)");
    }

}
