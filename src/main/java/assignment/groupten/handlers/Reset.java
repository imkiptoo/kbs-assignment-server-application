package assignment.groupten.handlers;

import io.undertow.server.HttpServerExchange;
import assignment.groupten.Engine;
import assignment.groupten.Main;

public class Reset extends DefaultHandler {

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        super.handleRequest(exchange);

        Main.engine = new Engine();
        Main.activePatient = Main.engine.getPatientBean();

        send(exchange, true);
    }
}
