package assignment.groupten.memory;

import assignment.groupten.memory.jess.DynamicFact;
import jess.Fact;
import jess.Token;
import assignment.groupten.DataLoader;

import java.util.ArrayList;
import java.util.List;

public class DiagnosedInfections extends DynamicFact {

    public DiagnosedInfections() {
    }

    public static List<Infection> infections = new ArrayList<>();

    public DiagnosedInfections(String name, String code) {
        super(name, code);
    }

    public void addInfection(Token infection){
        try{
            Fact latestFact = infection.topFact();
            infections.add(DataLoader.findInfectionByCode(
                    latestFact.getSlotValue("code").toString()));
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public Iterable getAll(){
        return infections;
    }

    public void clear(){
        infections.clear();
        infections = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "DiagnosedInfections{" +
                "infections=" + infections +
                '}';
    }
}
