package assignment.groupten.memory;

import assignment.groupten.memory.jess.DynamicFact;

public class Infection extends DynamicFact {

    private String infectionName;
    private String infectionDescription;
    private String symptoms;
    private String causes;
    private String treatment;
    private String notes;

    public Infection(String name, String code, String infectionName, String infectionDescription, String symptoms, String causes, String treatment, String notes) {
        super(name, code);
        super.setInfectionName(infectionName);
        super.setInfectionDescription(infectionDescription);
        super.setSymptoms(symptoms);
        super.setCauses(causes);
        super.setTreatment(treatment);
        super.setNotes(notes);
    }

    public Infection(String name, String code) { super(name, code); }

    @Override
    public String toString() {
        return "\nInfection{" +
                "\ncode='" + getCode() + "'" +
                "\nname='" + getName() + "'}\n";
    }
}
