package assignment.groupten.memory;

import assignment.groupten.memory.jess.DynamicFact;

import java.io.Serializable;

public class Patient extends DynamicFact implements Serializable {
    private String age;
    private String gender;
    private String has_a_sudden_onset_of_dizziness;
    private String has_felt_hot_or_feverish;
    private String has_discomfort_when_lying_down;
    private String appetite;
    private String ear_infections_had_in_the_past_year;
    private String discharge_from_ear;
    private String has_sore_throat;
    private String feels_itchy_in_ears;
    private String has_running_nose;
    private String has_swelling_behind_the_ear;
    private String feels_pressure_in_ears;
    private String has_diarrhoea;
    private String is_exposed_to_loud_sounds;
    private String has_had_head_injury;
    private String has_had_any_cold_or_allergy_symptoms;
    private String smokes_or_has_ever_smoked_tobacco;
    private String has_gone_swimming;
    private String conditions_diagnosed_with;

    public Patient(){}

    public Patient(String name, String code) {
        super(name, code);
    }

    public String getAge() { return age; }
    public String getGender() { return gender; }
    public String getHas_a_sudden_onset_of_dizziness() { return has_a_sudden_onset_of_dizziness; }
    public String getHas_felt_hot_or_feverish() { return has_felt_hot_or_feverish; }
    public String getHas_discomfort_when_lying_down() { return has_discomfort_when_lying_down; }
    public String getAppetite() { return appetite; }
    public String getEar_infections_had_in_the_past_year() { return ear_infections_had_in_the_past_year; }
    public String getDischarge_from_ear() { return discharge_from_ear; }
    public String getHas_sore_throat() { return has_sore_throat; }
    public String getFeels_itchy_in_ears() { return feels_itchy_in_ears; }
    public String getHas_running_nose() { return has_running_nose; }
    public String getHas_swelling_behind_the_ear() { return has_swelling_behind_the_ear; }
    public String getFeels_pressure_in_ears() { return feels_pressure_in_ears; }
    public String getHas_diarrhoea() { return has_diarrhoea; }
    public String getIs_exposed_to_loud_sounds() { return is_exposed_to_loud_sounds; }
    public String getHas_had_head_injury() { return has_had_head_injury; }
    public String getHas_had_any_cold_or_allergy_symptoms() { return has_had_any_cold_or_allergy_symptoms; }
    public String getSmokes_or_has_ever_smoked_tobacco() { return smokes_or_has_ever_smoked_tobacco; }
    public String getHas_gone_swimming() { return has_gone_swimming; }
    public String getConditions_diagnosed_with() { return conditions_diagnosed_with; }


    public void setAge(String age) {
        pcs.firePropertyChange("age", this.age, age);
        this.age = age;
    }

    public void setGender(String gender) {
        pcs.firePropertyChange("gender", this.gender, gender);
        this.gender = gender;
    }

    public void setHas_a_sudden_onset_of_dizziness(String has_a_sudden_onset_of_dizziness) {
        pcs.firePropertyChange("has_a_sudden_onset_of_dizziness", this.has_a_sudden_onset_of_dizziness, has_a_sudden_onset_of_dizziness);
        this.has_a_sudden_onset_of_dizziness = has_a_sudden_onset_of_dizziness;
    }

    public void setHas_felt_hot_or_feverish(String has_felt_hot_or_feverish) {
        pcs.firePropertyChange("has_felt_hot_or_feverish", this.has_felt_hot_or_feverish, has_felt_hot_or_feverish);
        this.has_felt_hot_or_feverish = has_felt_hot_or_feverish;
    }

    public void setHas_discomfort_when_lying_down(String has_discomfort_when_lying_down) {
        pcs.firePropertyChange("has_discomfort_when_lying_down", this.has_discomfort_when_lying_down, has_discomfort_when_lying_down);
        this.has_discomfort_when_lying_down = has_discomfort_when_lying_down;
    }

    public void setAppetite(String appetite) {
        pcs.firePropertyChange("appetite", this.appetite, appetite);
        this.appetite = appetite;
    }

    public void setEar_infections_had_in_the_past_year(String ear_infections_had_in_the_past_year) {
        pcs.firePropertyChange("ear_infections_had_in_the_past_year", this.ear_infections_had_in_the_past_year, ear_infections_had_in_the_past_year);
        this.ear_infections_had_in_the_past_year = ear_infections_had_in_the_past_year;
    }

    public void setDischarge_from_ear(String discharge_from_ear) {
        pcs.firePropertyChange("discharge_from_ear", this.discharge_from_ear, discharge_from_ear);
        this.discharge_from_ear = discharge_from_ear;
    }

    public void setHas_sore_throat(String has_sore_throat) {
        pcs.firePropertyChange("has_sore_throat", this.has_sore_throat, has_sore_throat);
        this.has_sore_throat = has_sore_throat;
    }

    public void setFeels_itchy_in_ears(String feels_itchy_in_ears) {
        pcs.firePropertyChange("feels_itchy_in_ears", this.feels_itchy_in_ears, feels_itchy_in_ears);
        this.feels_itchy_in_ears = feels_itchy_in_ears;
    }

    public void setHas_running_nose(String has_running_nose) {
        pcs.firePropertyChange("has_running_nose", this.has_running_nose, has_running_nose);
        this.has_running_nose = has_running_nose;
    }

    public void setHas_swelling_behind_the_ear(String has_swelling_behind_the_ear) {
        pcs.firePropertyChange("has_swelling_behind_the_ear", this.has_swelling_behind_the_ear, has_swelling_behind_the_ear);
        this.has_swelling_behind_the_ear = has_swelling_behind_the_ear;
    }

    public void setFeels_pressure_in_ears(String feels_pressure_in_ears) {
        pcs.firePropertyChange("feels_pressure_in_ears", this.feels_pressure_in_ears, feels_pressure_in_ears);
        this.feels_pressure_in_ears = feels_pressure_in_ears;
    }

    public void setHas_diarrhoea(String has_diarrhoea) {
        pcs.firePropertyChange("has_diarrhoea", this.has_diarrhoea, has_diarrhoea);
        this.has_diarrhoea = has_diarrhoea;
    }

    public void setIs_exposed_to_loud_sounds(String is_exposed_to_loud_sounds) {
        pcs.firePropertyChange("is_exposed_to_loud_sounds", this.is_exposed_to_loud_sounds, is_exposed_to_loud_sounds);
        this.is_exposed_to_loud_sounds = is_exposed_to_loud_sounds;
    }

    public void setHas_had_head_injury(String has_had_head_injury) {
        pcs.firePropertyChange("has_had_head_injury", this.has_had_head_injury, has_had_head_injury);
        this.has_had_head_injury = has_had_head_injury;
    }

    public void setHas_had_any_cold_or_allergy_symptoms(String has_had_any_cold_or_allergy_symptoms) {
        pcs.firePropertyChange("has_had_any_cold_or_allergy_symptoms", this.has_had_any_cold_or_allergy_symptoms, has_had_any_cold_or_allergy_symptoms);
        this.has_had_any_cold_or_allergy_symptoms = has_had_any_cold_or_allergy_symptoms;
    }

    public void setSmokes_or_has_ever_smoked_tobacco(String smokes_or_has_ever_smoked_tobacco) {
        pcs.firePropertyChange("smokes_or_has_ever_smoked_tobacco", this.smokes_or_has_ever_smoked_tobacco, smokes_or_has_ever_smoked_tobacco);
        this.smokes_or_has_ever_smoked_tobacco = smokes_or_has_ever_smoked_tobacco;
    }

    public void setHas_gone_swimming(String has_gone_swimming) {
        pcs.firePropertyChange("has_gone_swimming", this.has_gone_swimming, has_gone_swimming);
        this.has_gone_swimming = has_gone_swimming;
    }

    public void setConditions_diagnosed_with(String conditions_diagnosed_with) {
        pcs.firePropertyChange("conditions_diagnosed_with", this.conditions_diagnosed_with, conditions_diagnosed_with);
        this.conditions_diagnosed_with = conditions_diagnosed_with;
    }

    @Override
    public String toString() {
        return "Patient{" +
                ", \nage=" + age +
                ", \ngender=" + gender +
                ", \nhas_a_sudden_onset_of_dizziness=" + has_a_sudden_onset_of_dizziness +
                ", \nhas_felt_hot_or_feverish=" + has_felt_hot_or_feverish +
                ", \nhas_discomfort_when_lying_down=" + has_discomfort_when_lying_down +
                ", \nappetite=" + appetite +
                ", \near_infections_had_in_the_past_year=" + ear_infections_had_in_the_past_year +
                ", \ndischarge_from_ear=" + discharge_from_ear +
                ", \nhas_sore_throat=" + has_sore_throat +
                ", \nfeels_itchy_in_ears=" + feels_itchy_in_ears +
                ", \nhas_running_nose=" + has_running_nose +
                ", \nhas_swelling_behind_the_ear=" + has_swelling_behind_the_ear +
                ", \nfeels_pressure_in_ears=" + feels_pressure_in_ears +
                ", \nhas_diarrhoea=" + has_diarrhoea +
                ", \nis_exposed_to_loud_sounds=" + is_exposed_to_loud_sounds +
                ", \nhas_had_head_injury=" + has_had_head_injury +
                ", \nhas_had_any_cold_or_allergy_symptoms=" + has_had_any_cold_or_allergy_symptoms +
                ", \nsmokes_or_has_ever_smoked_tobacco=" + smokes_or_has_ever_smoked_tobacco +
                ", \nhas_gone_swimming=" + has_gone_swimming +
                ", \nconditions_diagnosed_with=" + conditions_diagnosed_with +
                '}';
    }
}
