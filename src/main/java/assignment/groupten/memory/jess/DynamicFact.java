package assignment.groupten.memory.jess;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class DynamicFact extends ShadowFact {

    public DynamicFact(){

    }

    public DynamicFact(String name, String code){
        super(name, code);
    }

    @JsonIgnore
    public PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener p) {
        pcs.addPropertyChangeListener(p);
    }
    public void removePropertyChangeListener(PropertyChangeListener p) {
        pcs.removePropertyChangeListener(p);
    }

}
