package assignment.groupten.memory.jess;

public class ShadowFact {
    private String name;
    private String code;
    private String infectionName;
    private String infectionDescription;
    private String symptoms;
    private String causes;
    private String treatment;
    private String notes;

    public ShadowFact(){}

    public ShadowFact(String name, String code){
        this.name = name;
        this.code = code;
    }

    public String getName() { return name; }
    public String getCode() { return code; }
    public String getInfectionName() { return infectionName; }
    public String getInfectionDescription() { return infectionDescription; }
    public String getSymptoms() { return symptoms; }
    public String getCauses() { return causes; }
    public String getTreatment() { return treatment; }
    public String getNotes() { return notes; }

    public void setName(String name) { this.name = name; }
    public void setCode(String code) { this.code = code; }
    public void setInfectionName(String infectionName) { this.infectionName = infectionName; }
    public void setInfectionDescription(String infectionDescription) { this.infectionDescription = infectionDescription; }
    public void setSymptoms(String symptoms) { this.symptoms = symptoms; }
    public void setCauses(String causes) { this.causes = causes; }
    public void setTreatment(String treatment) { this.treatment = treatment; }
    public void setNotes(String notes) { this.notes = notes; }
}
