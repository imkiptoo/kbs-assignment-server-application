package assignment.groupten.utils;

import assignment.groupten.handlers.DefaultHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.StatusCodes;

public class FallBack extends DefaultHandler {
    @Override
    public void handleRequest(HttpServerExchange exchange) {
        send(exchange, new ExceptionRepresentation(
                "URI Not Found",
                exchange.getRequestURI(),
                "URI "+exchange.getRequestURI()+" not found on server",
                StatusCodes.NOT_FOUND,
                exchange.getRequestMethod()
        ), StatusCodes.NOT_FOUND);
    }
}
