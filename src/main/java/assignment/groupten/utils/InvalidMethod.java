package assignment.groupten.utils;

import assignment.groupten.handlers.DefaultHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.StatusCodes;

public class InvalidMethod extends DefaultHandler {

    @Override
    public void handleRequest(HttpServerExchange exchange) {
        send(exchange, new ExceptionRepresentation(
                "Method Not Allowed",
                exchange.getRequestURI(),
                "Method "+exchange.getRequestMethod()+" not allowed",
                StatusCodes.METHOD_NOT_ALLOWED,
                exchange.getRequestMethod()
        ), StatusCodes.METHOD_NOT_ALLOWED);
    }
}
